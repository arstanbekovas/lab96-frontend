import axios from '../../axios-api';
import {push} from "react-router-redux";
import {
  CREATE_COCKTAIL_SUCCESS, DELETE_COCKTAIL_SUCCESS, FETCH_COCKTAIL_SUCCESS,
  FETCH_COCKTAILS_SUCCESS
} from "./actionTypes";
import {NotificationManager} from "react-notifications";

export const fetchCocktailsSuccess = cocktails => {
  return {type: FETCH_COCKTAILS_SUCCESS, cocktails};
};

export const fetchCocktailSuccess = cocktail => {
  return {type: FETCH_COCKTAIL_SUCCESS, cocktail};
};

export const deleteCocktailSuccess = cocktail => {
  return {type: DELETE_COCKTAIL_SUCCESS, cocktail};
};

export const fetchCocktails = () => {
  return (dispatch, getState) => {
    const user = getState().users.user;
    const isAdmin = user && user.role === 'admin';
    axios.get(isAdmin ? '/cocktails/admin' : '/cocktails').then(
      response => dispatch(fetchCocktailsSuccess(response.data))
    );
  }
};

export const fetchCocktail = (id) => {
  return dispatch => {
    axios.get('/cocktails/' + id).then(
      response => dispatch(fetchCocktailSuccess(response.data))
    );
  }
};

export const createCocktailSuccess = () => {
  return {type: CREATE_COCKTAIL_SUCCESS};
};


export const createCocktail = cocktailData => {
  return (dispatch, getState) => {
    const headers = {'Token': getState().users.token};
    return axios.post('/cocktails', cocktailData, {headers}).then(
      response => {
        dispatch(createCocktailSuccess());
        dispatch(push('/'));
        NotificationManager.success('Success', 'New cocktail created successfully and under moderator approval');
      },
      error => {
        NotificationManager.error('Error', 'Could not create new cocktail');
      }
    );
  };
};

export const cocktailPublished = (id) => {
  return (dispatch, getState) => {
    axios.put('/cocktails/' + id, {}, {headers: {'Token': getState().users.token}}).then(
      response => dispatch(fetchCocktail(id))
    );
    NotificationManager.success('Success', 'Cocktail published');
  }
};

export const cocktailDelete = (id) => {
  return (dispatch, getState) => {
    axios.delete('/cocktails/' + id, {headers: {'Token': getState().users.token}}).then(
      response => {
        dispatch(deleteCocktailSuccess());
        dispatch(push('/'));
        NotificationManager.success('Success', 'Deleted successfully');
      },
      error => {
        NotificationManager.error('Error', 'Could not delete cocktail');
      }
    );
  };
};

export const onStarClick = (id) => {
  return (dispatch, getState) => {
    axios.put('/cocktails/' + id, {}, {headers: {'Token': getState().users.token}}).then(
      response => dispatch(fetchCocktail(id))
    );
    NotificationManager.success('Success', 'Cocktail deleted');
  }
};