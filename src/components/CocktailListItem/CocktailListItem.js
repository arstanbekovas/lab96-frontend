import React from 'react';
import {Image, Panel} from "react-bootstrap";
import {NavLink} from "react-router-dom";
import PropTypes from 'prop-types';

import config from '../../config';

import notFound from '../../assets/images/not-found.png';

const CocktailListItem = props => {
  let image = notFound;

  if (props.image) {
    image = config.apiUrl + '/uploads/' + props.image;
  }
  let sum = 0;
  let average = 0;
  let count = 0;
  if(props.rate) {
    props.rate.forEach(rate => {
      sum += rate.rate;
      count = props.rate.length;
      average = sum / count;
    });
  }
  return (
    <NavLink to={'/cocktails/' + props.id}>
    <Panel>
      <Panel.Body>
        <Image
          style={{width: '100px', marginRight: '10px'}}
          src={image}
          thumbnail
        />
        <span> Name:
          {props.title}
        </span>

        <span
          style={{marginLeft: '10px', marginRight: '10px'}}>
          Rating: {average} ({count} votes)
        </span>

      </Panel.Body>
    </Panel>
    </NavLink>
  );
};

CocktailListItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  rate: PropTypes.array,
  image: PropTypes.string,
  published: PropTypes.bool
};

export default CocktailListItem;