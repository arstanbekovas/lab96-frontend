import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import FormElement from "../UI/Form/FormElement";
import './CocktailForm.css'

class CocktailForm extends Component {
  state = {
    recipe: '',
    title: '',
    ingredients: [
      {
        title: '',
        amount: ''
      }
    ],
    image: ''
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      if(key === 'ingredients') {
        formData.append(key, JSON.stringify(this.state[key]));
      } else {
        formData.append(key, this.state[key]);
      }
    });

    this.props.onSubmit(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  ingredientTitleChangeHandler = (event, id) => {

    const ingrs = [...this.state.ingredients];
    const ingr = {...ingrs[id]};
    ingr.title = event.target.value;
    ingrs[id] = ingr;
    this.setState({
      ingredients: ingrs
    });
  };

  ingredientAmountChangeHandler = (event, id) => {
    const ingrs = [...this.state.ingredients];
    const ingr = {...ingrs[id]};
    ingr.amount = event.target.value;
    ingrs[id] = ingr;
    this.setState({
      ingredients: ingrs
    });
  };
  ingredientDeleteHandler = (event, id) => {
    const ingredients = [...this.state.ingredients];
    ingredients.splice(id, 1);
    this.setState({ingredients});
  };
  addIngredient = () => {
    const ingredients = [...this.state.ingredients];
    ingredients.push({ title: '', amount: ''});
    this.setState({ingredients});
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  render() {

    return (
      <Form horizontal onSubmit={this.submitFormHandler}>
        <div>
          {this.state.ingredients.map((ingredient, i) => {
            return (
              <div>
                <FormElement className='form-control'
                  key={ingredient[i]}
                  propertyName="ingredients title"
                  title="Cocktail ingredients title"
                  type="text"
                  value={ingredient.title}
                  changeHandler={(e) => this.ingredientTitleChangeHandler(e, i)}
                  required
                />
                <FormElement className='form-control'
                  key={ingredient[i]}
                  propertyName="ingredients amount"
                  title="Cocktail ingredients amount"
                  type="text"
                  value={ingredient.amount}
                  changeHandler={(e) => this.ingredientAmountChangeHandler(e, i)}
                  required
                />
                <Button
                  className='delete'
                  bsStyle="danger"
                  type="submit"
                  onClick={this.ingredientDeleteHandler}
                >X</Button>
              </div>
            )
          })
          }
            <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button onClick={this.addIngredient} bsStyle="primary" type="submit">Add ingredient</Button>
            </Col>
          </FormGroup>
        </div>

        <FormElement
          propertyName="title"
          title="Cocktail title"
          type="text"
          value={this.state.title}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="recipe"
          title="Cocktail recipe"
          type="textarea"
          value={this.state.recipe}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="image"
          title="Cocktail image"
          type="file"
          changeHandler={this.fileChangeHandler}
        />

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="success" type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default CocktailForm;
