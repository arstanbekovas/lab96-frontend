import React from 'react';
import {Image, Panel} from "react-bootstrap";
import PropTypes from 'prop-types';

import config from '../../config';

import notFound from '../../assets/images/not-found.png';

const CocktailDetailItem = props => {
  let image = notFound;

  if (props.image) {
    image = config.apiUrl + '/uploads/' + props.image;
  }

  let sum = 0;
  let average = 0;
  let count = 0;
  if(props.rate) {
    props.rate.forEach(rate => {
      sum += rate.rate;
      count = props.rate.length;
      average = sum / count;
    });
  }

  return (
    <Panel>
      <Panel.Body>
        <Image
          style={{width: '30%', marginRight: '10px'}}
          src={image}
          thumbnail
        />
        <span style={{fontSize: '20px', marginRight: '2%', marginLeft: '2%'}}>
          <b>Name: {props.title}</b>
        </span>
        <span style={{fontSize: '20px', marginRight: '2%', marginLeft: '2%'}}>
         <b>Rating: </b> {average} ({count} votes)
        </span>
        <span style={{fontSize: '20px', marginRight: '2%', marginLeft: '2%'}}>
          <b>Ingredients:</b>
          <ul>{props.ingredients.map(ing => (
            <li>{ing.title} : {ing.amount}</li>
          ))}</ul>
        </span>
        <div style={{fontSize: '20px', marginRight: '2%', marginLeft: '2%'}}>
          <b>Recipe: {props.recipe}</b>
        </div>
        <span>
          {props.published}
        </span>

      </Panel.Body>
    </Panel>
  );
};

CocktailDetailItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  image: PropTypes.string,
  ingredients: PropTypes.array,
  recipe: PropTypes.string,
  rate: PropTypes.array,
  published: PropTypes.bool
};

export default CocktailDetailItem;