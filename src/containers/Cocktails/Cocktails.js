import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader} from "react-bootstrap";
import {fetchCocktails} from "../../store/actions/cocktails";
import {Link} from "react-router-dom";

import CocktailListItem from '../../components/CocktailListItem/CocktailListItem';

class Cocktails extends Component {
  componentDidMount() {
    this.props.onFetchCocktails();
  }

  render() {
    return (
      <Fragment>
        <PageHeader>
          Cocktails
          { this.props.user && this.props.user.role === 'admin' &&
            <Link to="/cocktails/new">
              <Button bsStyle="primary" className="pull-right">
                Add cocktail
              </Button>
            </Link>
          }
        </PageHeader>

        {this.props.cocktails.map(cocktail => (
          <CocktailListItem
            key={cocktail._id}
            id={cocktail._id}
            title={cocktail.title}
            image={cocktail.image}
            pubshed={cocktail.published}
            rate={cocktail.rate}
          />
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    cocktails: state.cocktails.cocktails,
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchCocktails: () => dispatch(fetchCocktails())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Cocktails);