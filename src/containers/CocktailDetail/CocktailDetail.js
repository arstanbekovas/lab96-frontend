import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader} from "react-bootstrap";
import {cocktailDelete, cocktailPublished, fetchCocktail, onStarClick} from "../../store/actions/cocktails";
import StarRatingComponent from 'react-star-rating-component';

import CocktailDetailItem from "../../components/CocktailDetailItem/CocktailDetailItem";

class CocktailDetail extends Component {
  state = {
    rating: ''
  };

  componentDidMount() {
    this.props.onFetchCocktail(this.props.match.params.id);
  }

  onStarClick = (nextValue, prevValue, name) => {
    this.setState({rating: nextValue});
  };

  publish = () => {
    this.props.onPublishCocktail(this.props.cocktail._id);
  };

  deleteCocktail = () => {
    this.props.onDeleteCocktail(this.props.cocktail._id);
  };

  render() {
    if (!this.props.cocktail) {
      return <div>Loading...</div>
    }

    const { rating } = this.state;

    return (
      <Fragment>
        <PageHeader>
          Cocktails
        </PageHeader>
         <CocktailDetailItem
            key={this.props.cocktail._id}
            id={this.props.cocktail._id}
            title={this.props.cocktail.title}
            ingredients={this.props.cocktail.ingredients}
            recipe={this.props.cocktail.recipe}
            rate={this.props.cocktail.rate}
            image={this.props.cocktail.image}
            published={this.props.cocktail.published}
          />
        { this.props.user && this.props.user.role === 'admin' &&
        <Fragment>
          {this.props.cocktail.published === false ?
             <Button style={{marginLeft: '10px', marginRight: '10px'}}
              bsStyle="success"
              type="submit"
              onClick={this.publish}>Publish</Button>
            : <span>This cocktail published</span>
          }

          <Button
            style={{marginLeft: '10px', marginRight: '10px'}}
            bsStyle="danger"
            type="submit"
            onClick={this.deleteCocktail}>Delete</Button>
        </Fragment>
        }
        <div>
          <h2>Rating from state: {rating}</h2>
          <StarRatingComponent
            name="rate1"
            starCount={10}
            value={rating}
            onStarClick={this.onStarClick}
          />
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    cocktail: state.cocktails.cocktail,
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchCocktail: (id) => dispatch(fetchCocktail(id)),
    onPublishCocktail: (id) => dispatch(cocktailPublished(id)),
    onDeleteCocktail: (id) => dispatch(cocktailDelete(id)),
    onStarClick: (id) => dispatch(onStarClick(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CocktailDetail);