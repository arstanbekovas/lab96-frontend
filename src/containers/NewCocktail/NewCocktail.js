import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import CocktailForm from "../../components/CocktailForm/CocktailForm";
import {createCocktail} from "../../store/actions/cocktails";

class NewCocktail extends Component {

  createCocktail = cocktailData => {
    this.props.onCocktailCreated(cocktailData);
  };

  render() {
    return (
      <Fragment>
        <PageHeader>Add new cocktail</PageHeader>
        <CocktailForm
          onSubmit={this.createCocktail}
        />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onCocktailCreated: cocktailData => {
    return dispatch(createCocktail(cocktailData))
  }
});


export default connect(null, mapDispatchToProps)(NewCocktail);