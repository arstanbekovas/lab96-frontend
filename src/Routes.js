import React from 'react';
import {Route, Switch} from "react-router-dom";

import NewCocktail from "./containers/NewCocktail/NewCocktail";
import CocktailDetail from "./containers/CocktailDetail/CocktailDetail";
import Register from "./containers/Register/Register";
import Cocktails from "./containers/Cocktails/Cocktails";
import Login from "./containers/Login/Login";


const Routes = () => (
  <Switch>
    <Route path="/" exact component={Cocktails}/>
    <Route path="/cocktails/new" exact component={NewCocktail}/>
    <Route path="/cocktails/:id" exact component={CocktailDetail}/>
    <Route path="/register" exact component={Register}/>
    <Route path="/login" exact component={Login}/>
  </Switch>
);

export default Routes;